Import("env")

# access to global construction environment
#print(env)

# Dump construction environment (for debug purpose)
#print(env.Dump())

env.Execute("/usr/bin/perl bin/html_to_include.pl html/*.html")