# ESP-WebCam

Basic webcam, based on the Espressif Systems (Shanghai)'s (https://www.espressif.com) 
Example Code and extended (and simplified with face recognition code removed).

The original codebase was taken from here: https://github.com/espressif/arduino-esp32/tree/master/libraries/ESP32/examples/Camera/CameraWebServer
under copyright of Espressif 

See LICENSE.md file in this repository for license information.
