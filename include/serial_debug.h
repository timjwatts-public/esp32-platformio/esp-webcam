#ifdef DEBUG
#define DEBUG_SERIAL_PRINTF 1
#else
#define DEBUG_SERIAL_PRINTF 0
#endif

#define DEBUGF(fmt, ...) do { if (DEBUG_SERIAL_PRINTF) Serial.printf("%s:%d::%s() " fmt "\n", __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while (0)
