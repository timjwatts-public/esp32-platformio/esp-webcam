Applicable Licenses:

- (Default License) GNU LGPL License file, v2.1 https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html
- Apache v2.0 License http://www.apache.org/licenses/LICENSE-2.0

Any file should be assumed to be under the LGPL v2.1 License unless otherwise stated within the file.

Please note that code from the Arduino ESP32 framework is also linked in so that license applies too.

CAVEATS:

If Espressif find this repository and disagree, I'll be happy to discuss License clarification with them, so we will proceed in good faith.

It is certainly clear their code is released as open source - our only confusion is the specific license.

However, if you base a commercial product off this code, it is YOUR RESPONSIBILITY to ensure lawful compliance with licensing and copyright laws.
